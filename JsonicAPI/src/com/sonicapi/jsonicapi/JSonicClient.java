package com.sonicapi.jsonicapi;

import java.io.File;

import net.sourceforge.javajson.JsonException;
import net.sourceforge.javajson.JsonObject;



public class JSonicClient {
	
	private static final String sAccessId = "<INSERT YOUR sonicAPI.com ACCESS ID HERE>";
	
	// The URL for the service
	private static final String sEndpoint = "http://api.sonicAPI.com/";
	
	// REST method names
	private static final String sAuftaktURL = "analyze/tempo";
	private static final String sFileUpload = "file/upload";
		
	private static RestClient client;
		
	/**
	 * Uploads a file and, if successful, sets the local file ID
	 * for further use.
	 * 
	 * @param fileToUpload	absolute path pointing to the file which will be uploaded
	 */
	public static String uploadFile (String fileToUpload) {
		
		String endPointUpload = sEndpoint + sFileUpload;
		
		client = new RestClient(endPointUpload);
		client.setFile(new File(fileToUpload));
		
		client.addParam("access_id", sAccessId);
		
		String fileId = null;
		
		try {
		    
			client.execute(RestClient.RequestMethod.POST);
		
			String response = client.getResponse();
			
			JsonObject json = JsonObject.parse(response);
			JsonObject uploadFileID = json.getJsonObject("file");
			
			fileId = uploadFileID.getString("file_id"); 			
			
			
		} catch (JsonException e) {
			
			e.printStackTrace();

		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return fileId;
						
	}
		
	
	/**
	 * Performs the analyze/tempo task
	 * 
	 * @param fileId	a file ID for a successfully uploaded file
	 * 
	 * @return 			a String containing the server's response 
	 */
	public static String analyzeTempo(String fileId) {
		
		String endPointUpload = sEndpoint + sAuftaktURL;
		
		client = new RestClient(endPointUpload);
				
		client.addParam("access_id", sAccessId);
		client.addParam("input_file", fileId);
				    
		try {
			
			client.execute(RestClient.RequestMethod.POST);
					
		} catch (Exception e) {
			
			e.printStackTrace();
			
		} 
			
		return client.getResponse();
			
	}
		
	
	public static void main(String[] args) {
	
		String myFilePath = "<INSERT ABSOLUTE PATH TO LOCAL SOUND FILE>";
		
		// Upload file
		String fileId = JSonicClient.uploadFile(myFilePath);
			
		// Analyze the tempo, using the file ID that was set by uploadFile()
		String tempoResponse = JSonicClient.analyzeTempo(fileId);
				
						
	}
	
}